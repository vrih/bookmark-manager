use std::fs::File;

use select::document::Document;
use select::node::Node;
use select::predicate::*;

use std::io::Write;
use std::time::Duration;

use reqwest::Client;
use reqwest::get;
use reqwest::header;

use serde_json;
use serde_json::Value;
use reqwest;
use url::Url;
use log::{info, error};

#[derive(Debug, Clone, PartialEq)]
struct Icon{
    x: u16,
    y: u16,
    href: String,
    poor: bool
}

const DESKTOP_UA: &str = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 Safari/537.36";

const MOBILE_UA: &str = "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Mobile Safari/537.36";
const TIMEOUT: u64 = 10;

fn split_x_y(sizes: &str) -> (u16, u16) {
    let x_y = sizes.split('x').collect::<Vec<&str>>();
    if x_y.len() != 2 {
        return (1, 1)
    }
    let x_y_parsed = x_y.iter().map(|&x| x.parse::<u16>().unwrap()).collect::<Vec<u16>>();
    (x_y_parsed[0], x_y_parsed[1])
}

fn attr_parser(doc: &Document, url: &str) -> Option<Vec<Icon>>{
    let mut links: Vec<Icon> = Vec::new();

    for link in doc.find(Name("link")).collect::<Vec<Node>>(){
        let href = match link.attr("href"){
            Some(s) => {
                if s.is_empty() {continue};
                if !s.ends_with("png") && !s.ends_with("svg") {
                    continue
                };
                s
            },
            None => continue
        };

        let (x, y) = split_x_y(link.attr("sizes").unwrap_or("1x1"));
        links.push(Icon{x, y, href: url_from_paths(url, href), poor: false});
    };

    if links.is_empty() {
        return None
    }

    Some(links)
}

fn icons_from_manifest(url: &str, data: &str) -> Option<Vec<Icon>>{
    let mut icons: Vec<Icon> = Vec::new();
    match serde_json::from_str(data) {
        Ok(a) => {
            let b: Value = a;
            if let Some(x) = b["icons"].as_array() {
                for link in x.iter() {
                    let sizes = link["sizes"].as_str().unwrap();
                    let (x, y) = split_x_y(sizes);

                    let mut src = String::from(link["src"].as_str().unwrap());
                    if !src.starts_with("http") {
                        let mut divider = "";
                        if !url.ends_with("/") && !&src.starts_with("/") {
                            divider = "/";
                        }

                        src = url.to_owned() + &divider +  &src;
                    }
                    icons.push(Icon{x, y, href: src, poor:false});
                };

                if icons.is_empty() {
                    return None
                }
                return Some(icons)
            }
            None
        },
        _ => None
    }
}

fn og_parser(doc: &Document, url: &str) -> Option<Vec<Icon>>{
    let mut links: Vec<Icon> = Vec::new();
    for link in doc.find(Name("meta").and(Attr("property", "og:image"))).collect::<Vec<Node>>(){
        let path = link.attr("content").unwrap();
        if path.is_empty() {
            continue
        }
        links.push(Icon{x: 1, y: 1, href: url_from_paths(url, path), poor: true});
    };

    for link in doc.find(Name("meta").and(Attr("name", "twitter:image"))).collect::<Vec<Node>>(){
        let path = link.attr("content").unwrap();
        if path.is_empty() {
            continue
        }
        links.push(Icon{x: 1200, y: 2, href: url_from_paths(url, path), poor: true});
    };
    if links.is_empty() {
        return None
    }
    Some(links)
}

fn get_image_paths(doc: &Document, url: &str) -> Option<Vec<Icon>>{
    let attricons = attr_parser(doc,  url);
    if attricons.is_some() {
        return attricons
    }

    return og_parser(doc, url)
}

fn url_from_paths(root: &str, path: &str) -> String{
    if path.starts_with("http") {
       return path.to_string()
    }

    if path.starts_with("//") {
        let mut image_url = String::from(path);
        image_url.insert_str(0, "http:");
        return image_url
    }

    return Url::parse(root).unwrap().join(path).unwrap().as_str().to_string();
}

async fn document_for_ua(url: &str, ua: &str) -> Option<(String, Document)>{
    let mut headers = header::HeaderMap::new();
    headers.insert(header::USER_AGENT, header::HeaderValue::from_str(ua).unwrap());

    // get a client builder
    let client = Client::builder()
        .default_headers(headers)
        .timeout(Duration::new(TIMEOUT, 0))
        .build().expect("Bob");
    let resp = match client.get(url).send().await {
        Ok(a) => a,
        Err(err) => {
            error!("{:?}", err);
            return None
        }
    };

    let urlout = String::from(resp.url().as_str());
    let body = match resp.text().await {
        Ok(data) => data,
        Err(error) => {
            error!("{:?}", error);
            return None
        }
    };

    Some((urlout, Document::from(&body[..])))
}

// TODO: Change to Option
async fn get_manifest_json(url: &str, ua: &str) -> Option<String>{
    let mut headers = header::HeaderMap::new();
    headers.insert(header::USER_AGENT, header::HeaderValue::from_str(&ua.to_string()).unwrap());

    let mut root_url = Url::parse(url).unwrap();
    root_url.set_path("");
    let parsed = root_url.join("manifest.json").unwrap();

    // get a client builder
    let client = Client::builder()
        .default_headers(headers)
        .timeout(Duration::new(TIMEOUT, 0))
        .build().expect("Bob");
    let resp = match client.get(parsed.as_str()).send().await{
        Ok(data) => data,
        Err(error) => { 
            error!("{:?}", error );
            return None
        }
    };

    let body = match resp.text().await {
        Ok(data) => data,
        Err(error) => {
            error!("{}", &url);
            error!("{:?}", error );
            return None
        }
    };
    Some(body)
}

fn get_best_icon(all_icons: &mut Vec<Icon>, new_icons: &Vec<Icon>) -> Icon {
    all_icons.extend(new_icons.to_owned());
    all_icons.sort_by_key(|a| a.x);
    return all_icons.last().unwrap().to_owned();
}

async fn get_page_header_icons(url: &str) -> Option<Icon>{
    let mut all_icons: Vec<Icon> = Vec::new();

    let (final_url, document) = match document_for_ua(url, DESKTOP_UA).await {
        Some(a) => a,
        None => return None
    };
    let best_desktop_icon = get_image_paths(&document, &final_url)
        .map(|i| get_best_icon(&mut all_icons, &i));

    if let Some(icon) = best_desktop_icon {
        if icon.x > 128 {
            return Some(icon)
        }
    };

    let (final_urlb, documentb) = match document_for_ua(url, MOBILE_UA).await {
        Some(a) => a,
        None => return None}
    ;
    return get_image_paths(&documentb, &final_urlb)
              .map(|i| get_best_icon(&mut all_icons, &i));
}

async fn get_icon_objects(url: &str) -> Option<Icon> {
    let icon = match get_manifest_json(url, DESKTOP_UA).await{
        Some(data) => {
            let icon = icons_from_manifest(&url, &data).map(|i| {
                let mut all_icons: Vec<Icon> = Vec::new();
                get_best_icon(&mut all_icons, &i)
            });
            icon
        },
        None => None
    };

    match icon {
        Some(_) => icon,
        None => get_page_header_icons(url).await
    }
}

pub async fn download_image(url: &str, fs_path: &str){
    let icon_url = get_icon_objects(url).await.map(|i| i.href.clone());

    if let Some(url) = icon_url {
        download_media(&url, fs_path).await
    } else {
        info!("No image for {}", url)
    }
}

fn replace_extension(source: &str, dest: &str) -> String {
    if source.ends_with("svg") {
        return dest.replace(".png", ".svg");
    }
    return dest.to_owned();
}

pub async fn download_media(url: &str, fs_path: &str) {
    let resp = get(url).await.unwrap().bytes().await.unwrap();
    let amended_path = replace_extension(url, fs_path);
    let mut f = File::create(amended_path).unwrap();
    f.write_all(&resp).unwrap();
    info!("Downloaded {}", url);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn split_x_y_test(){
        assert_eq!((1, 2), split_x_y("1x2"));
        assert_eq!((1, 1), split_x_y("any"));
    }

    #[test]
    fn icons_from_manifest_test(){
        assert_eq!(vec![
            Icon{x: 114, y: 114, href: String::from("https://assets-cdn.github.com/apple-touch-icon-114x114.png"), poor: false},
            Icon{x: 120, y: 120, href: String::from("https://assets-cdn.github.com/apple-touch-icon-120x120.png"), poor: false}],
                   icons_from_manifest("http://www.example.com", "{\"name\":\"GitHub\",\"icons\":[{\"sizes\":\"114x114\",\"src\":\"https://assets-cdn.github.com/apple-touch-icon-114x114.png\"},{\"sizes\":\"120x120\",\"src\":\"https://assets-cdn.github.com/apple-touch-icon-120x120.png\"}]}").unwrap())}

    #[test]
    fn attr_parser_test(){
        let doc1 = Document::from("<html><head><link rel=\"icon\" sizes=\"192x192\" href=\"/1.png\"/></head></html>");
        assert_eq!(Some(vec![Icon{x: 192, y: 192, href: "http://example.com/1.png".to_string(), poor: false}]), attr_parser(&doc1, "http://example.com"));
        
        let doc2 = Document::from("<html><head><link rel=\"icon\" sizes=\"192x192\" href=\"/1.bad\"/></head></html>");
        assert_eq!(None, attr_parser(&doc2, "http://example.com"));
    }

    #[test]
    fn url_from_paths_test(){
        assert_eq!("https://www.example.com/123",
                   url_from_paths("https://www.example.com", "123"));
        assert_eq!("http://www.example2.com/123",
                   url_from_paths("https://www.example.com", "http://www.example2.com/123"));
        assert_eq!("http://www.example2.com/123",
                   url_from_paths("https://www.example.com", "//www.example2.com/123"));
    }

    #[test]
    fn replace_extension_test() {
        assert_eq!("123.png", replace_extension("abc.png", "123.png"));
        assert_eq!("123.svg", replace_extension("abc.svg", "123.png"));
        assert_eq!("123.png", replace_extension("abc.ico", "123.png"));
    }

    #[test]
    fn get_best_icon_test() {
        let mut all_icons: Vec<Icon> = Vec::new();
        let icons = vec![
            Icon{x: 2, y: 2, href:"a".to_string(), poor: false},
            Icon{x: 1, y: 1, href:"a".to_string(), poor: false}
        ];
        assert_eq!(Icon{x: 2, y: 2, href: "a".to_string(), poor: false},
                   get_best_icon(&mut all_icons, &icons));
        assert_eq!(2, all_icons.len());
        assert_eq!(Icon{x: 2, y: 2, href: "a".to_string(), poor: false},
                   get_best_icon(&mut all_icons, &icons));
        assert_eq!(4, all_icons.len());
    }

    // #[test]
    // fn get_mobile_icons_test() {
    //     let icon1 = Icon{x:1, y:1, href: "a".to_string(), poor: false};
    //     let icons = &[Icon{x:1, y:1, href: "a".to_string(), poor: false}];
    //     assert_eq!(
    //         Some(icon1),
    //         get_mobile_icons("a", Some(i
    //                                    clone()])));
    // }
}

