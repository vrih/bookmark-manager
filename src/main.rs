extern crate clap;
extern crate futures;

//icon
extern crate select;
extern crate reqwest;
extern crate url;
extern crate crossbeam_channel;
extern crate crossbeam_utils;
extern crate serde_json;

use structopt::StructOpt;

use std::io::prelude::*;
use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;
use std::fs::OpenOptions;
use std::env;
use lib::Bookmark;
use warp::Filter;

extern crate log;
use log::{info};
use futures::future::join_all;
use futures::executor::block_on;
mod icon;
mod lib;

fn list_bookmarks(path: &str) {
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);
    for line in file.lines() {
        match Bookmark::new_from_line(line.unwrap()){
            Ok(b) => println!("{}", b),
            Err(_) => panic!("error reading bookmark")
        }
    }
}

#[derive(Debug, StructOpt)]
#[structopt(about = "the stupid content tracker")]
enum Args {
    List {
        file: String
    },
    Add {
        #[structopt(short)]
        url: String,
        #[structopt(short="T")]
        title: String,
        #[structopt(short="t", default_value = "")]
        taglist: String,
        #[structopt(short, default_value = "")]
        custom_image: String,
        file: String
    },
    Serve {
        file: String
    },
    Html {
        file: String
    },
    Image {
        #[structopt(short)]
        all: bool,
        #[structopt(short)]
        label: String,
        file: String
    }
}
async fn add_bookmark(path: &str, url: &str, title: &str, tags: &str, custom_image: &str){
    let f = OpenOptions::new()
        .append(true)
        .open(path)
        .unwrap();

    let b = Bookmark::new_from_input(String::from(url), String::from(title), String::from(tags), String::from(custom_image));
    let c = b.output();
    writeln!(&f, "{}", c).unwrap();
    let fs_path = image_path(&b.hash);
    info!("{}", fs_path);
    update_image(url.to_string(), fs_path, b.title).await;
}

fn image_path(hash: &str) -> String{
    let image_path = env::var("RBM_BASE").expect("Set RBM_BASE env");

    format!("{}/.bm.shots/{}.png", &image_path, hash)
}

fn html_string(path: String) -> String {
    let mut bs: Vec<Bookmark> = Vec::new();
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);
    for line in file.lines(){
        match Bookmark::new_from_line(line.unwrap()){
            Ok(b) => bs.push(b),
            Err(_) => continue
        };
    }
    lib::html_output(bs)
}

fn output_html(path: &str){
    let directory_path = env::var("RBM_BASE").expect("Set RBM_BASE env");
    let directory_path = format!("{}/bm.html", directory_path);

    let fo = OpenOptions::new()
        .write(true)
        .truncate(true)
        .create(true)
        .open(directory_path)
        .unwrap();

    write!(&fo, "{}", html_string(path.to_string())).unwrap()
}

fn stream_html(path: String) -> impl warp::Reply{
    warp::reply::html(html_string(path.to_string()))
}

async fn update_image(path: String, fs_path: String, title: String){
    if path == "" || fs_path == "" {
        info!("Skipping {}", title);
        return
    }
    icon::download_image(&path, &fs_path).await
}

async fn refresh_all_images(path: String){
    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);
    let futures = file.lines().map(|line | {
        let l = line.unwrap();
        let mut u: String = "".to_string();
        let mut path: String = "".to_string();
        let mut title: String = "".to_string();
        match  Bookmark::new_from_line(l) {
            Ok(b) => {
                title = b.title;
                if b.custom_image.len() == 0 {
                    info!("Checking {}", title);
                    path = image_path(&b.hash);
                    u = b.url
                }
            },
            _ => {
        }
        };
        update_image(u, path, title)});
    join_all(futures).await;
}

async fn refresh_image(path: &str, label: &str){
    // refresh the image for an existing bookmark

    let f = File::open(path).unwrap();
    let file = BufReader::new(&f);
    for line in file.lines() {
        let b = match Bookmark::new_from_line(line.unwrap()){
            Ok(b) => b,
            Err(_) => continue
        };
        if b.label == label{
            let ip = image_path(&b.hash);
            block_on(update_image(b.url, ip, b.title));
        }
    }
}

async fn serve(path: String) {
  let string1 = path.clone();
  let string2 = path.clone();
  let base_path = env::var("RBM_BASE").unwrap();
  let root = warp::path::end()
      .map(move || stream_html(string1.to_owned()));

  let images = warp::path(".bm.shots").and(warp::fs::dir(format!("{}/.bm.shots/", base_path)));

  let scripts = warp::path("js").and(warp::fs::dir(format!("{}/js/", base_path)));
  let routes = root.or(images).or(scripts);

  warp::serve(routes)
      .run(([0, 0, 0, 0], 3030))
      .await;
}

#[tokio::main]
async fn main() {
    env_logger::init();
    match Args::from_args() {
       Args::List {file} => { list_bookmarks(&file) },
       Args::Html {file} => { output_html(&file) },
       Args::Add { url, title, taglist, custom_image, file} => {
           add_bookmark(&file, &url, &title, &taglist, &custom_image).await;
           output_html(&file)
       },
       Args::Serve {file} => { serve(file).await },
       Args::Image { all, label, file } => {
           if all {
             block_on(refresh_all_images(file))
           } else {
             refresh_image(&file, &label).await
           }
       },
    }
}
