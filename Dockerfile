FROM rust:1.50 as build

RUN USER=root cargo new --bin rbm
WORKDIR ./rbm

COPY Cargo.toml Cargo.lock ./
RUN cargo build --release
RUN rm src/*.rs

ADD src ./src

RUN cargo install --path .

FROM debian:buster-slim
RUN apt-get update && apt-get install libssl1.1
COPY --from=build /usr/local/cargo/bin/bookmark-manager .
RUN chown 1000 bookmark-manager && chgrp 1000 bookmark-manager
USER 1000
EXPOSE 3030
ENV RBM_BASE /data/

CMD ["/bookmark-manager", "serve", "/data/bm.lnk"]
